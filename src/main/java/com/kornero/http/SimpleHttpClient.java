package com.kornero.http;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Secret google request:
 * <pre>
 * {@code
 * https://www.google.ru/search?q=%2B%D0%93%D1%9E%D0%B2%E2%80%9A%C2%AC%D0%92%D1%9C:8080%D0%93%D1%9E%D0%B2%E2%80%9A%C2%AC%D0%92%D1%96+%2B%D0%93%D1%9E%D0%B2%E2%80%9A%C2%AC%D0%92%D1%9C:3128%D0%93%D1%9E%D0%B2%E2%80%9A%C2%AC%D0%92%D1%96+%2B%D0%93%D1%9E%D0%B2%E2%80%9A%C2%AC%D0%92%D1%9C:80%D0%93%D1%9E%D0%B2%E2%80%9A%C2%AC%D0%92%D1%96+filetype:txt&gws_rd=cr,ssl&ei=NkvOVKrgKseAywOViIG4DA#q=%2B%E2%80%9D:8080%E2%80%B3+%2B%E2%80%9D%3A3128%E2%80%B3+%2B%E2%80%9D%3A80%E2%80%B3+filetype%3Atxt&aq=f
 * }
 * </pre>
 */
@ThreadSafe
public class SimpleHttpClient {

    private static final Logger logger = LoggerFactory.getLogger(SimpleHttpClient.class);

    private static final Pattern PATTERN = Pattern.compile("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}).+?(\\d{2,5})");
    private static final int CONNECT_TIMEOUT = 3500;
    private static final int READ_TIMEOUT = 15000;

    private final BlockingQueue<Proxy> proxies = new LinkedBlockingQueue<>();
    private final AtomicInteger proxiesAmount = new AtomicInteger();

    public void init(final String... proxySources) {
        final ExecutorService executorService = Executors.newFixedThreadPool(15);
        for (final String proxySource : proxySources) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        for (final Map.Entry<String, Integer> proxyAddress : grabProxies(proxySource)) {
                            final InetSocketAddress sa = new InetSocketAddress(proxyAddress.getKey(), proxyAddress.getValue());
                            final Proxy proxy = new Proxy(Proxy.Type.HTTP, sa);
                            proxies.add(proxy);
                        }
                    } catch (final IOException e) {
                        logger.error("init(): bad proxy source='{}'", proxySource, e);
                    }
                }
            });
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(60, TimeUnit.MINUTES);
        } catch (final InterruptedException e) {
            logger.error("init(): ", e);
        }

        logger.info("init(): total proxies={}", this.proxies.size());
        proxiesAmount.set(this.proxies.size());
    }

    public void checkProxies() {
        logger.debug("checkProxies(): ");

        final ExecutorService executorService = Executors.newFixedThreadPool(100);
        final Set<Proxy> badProxies = Collections.newSetFromMap(new ConcurrentHashMap<Proxy, Boolean>());
        for (final Proxy proxy : proxies) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        final String html = getHtml("http://ya.ru", proxy);
                        if (StringUtils.isBlank(html)) {
                            final String html2 = getHtml("http://google.ru", proxy);
                            if (StringUtils.isBlank(html2)) {
                                badProxies.add(proxy);
                            }
                        }
                    } catch (final IOException e) {
                        badProxies.add(proxy);
                        logger.debug("checkProxies(): bad proxy='{}'", proxy);
                    }
                }
            });
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(60, TimeUnit.MINUTES);
        } catch (final InterruptedException e) {
            logger.error("checkProxies(): ", e);
        }

        proxies.removeAll(badProxies);

        logger.info("checkProxies(): total proxies={}", this.proxies.size());
        proxiesAmount.set(this.proxies.size());
    }

    @Nonnull
    public String getHtmlViaProxy(@Nonnull final String urlToRead) throws IOException, InterruptedException {
        final Proxy proxy = proxies.poll(1, TimeUnit.MINUTES);

        try {
            final String html = getHtml(urlToRead, proxy);
            proxies.add(proxy); // Return good proxy back into queue.
            return html;
        } catch (final IOException e) {
            logger.info("getHtmlViaProxy(): Remove proxy, now = {}", proxiesAmount.decrementAndGet());

            return getHtmlViaProxy(urlToRead);
        }
    }

    @Nonnull
    public String getHtml(@Nonnull final String urlToRead) throws IOException {
        return getHtml(urlToRead, null);
    }

    @Nonnull
    public String getHtml(@Nonnull final String urlToRead, @Nullable final Proxy proxy) throws IOException {
        final URL url = new URL(urlToRead);

        final HttpURLConnection conn;
        if (proxy == null) {
            conn = (HttpURLConnection) url.openConnection();
        } else {
            conn = (HttpURLConnection) url.openConnection(proxy);
        }

        conn.setRequestMethod("GET");
        conn.setConnectTimeout(CONNECT_TIMEOUT);
        conn.setReadTimeout(READ_TIMEOUT);

        // Setup request headers.
        conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
//        conn.setRequestProperty("Accept-Encoding", "gzip, deflate, sdch");
        conn.setRequestProperty("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,vi;q=0.2");
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36");
        conn.setRequestProperty("DNT", "1");

        final StringBuilder result = new StringBuilder();
        String line;
        try (final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        }

        return result.toString();
    }

    private Set<Map.Entry<String, Integer>> grabProxies(@Nonnull final String proxySource) throws IOException {
        final Set<Map.Entry<String, Integer>> proxies = new HashSet<>();

        final Matcher matcher = PATTERN.matcher(getHtml(proxySource));
        while (matcher.find()) {
            final String address = matcher.group(1);
            final int port = Integer.parseInt(matcher.group(2));
            if (0 < port && port < 65535) {
                proxies.add(new AbstractMap.SimpleImmutableEntry<>(address, port));
            }
        }

        logger.info("grabProxies(): amount={}, url='{}'", proxies.size(), proxySource);

        return proxies;
    }
}