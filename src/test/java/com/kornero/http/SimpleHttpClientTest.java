package com.kornero.http;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class SimpleHttpClientTest {

    private static final Logger logger = LoggerFactory.getLogger(SimpleHttpClientTest.class);

    private static final SimpleHttpClient SIMPLE_HTTP_CLIENT = new SimpleHttpClient();

    @Ignore
    @Test
    public void test1() throws IOException, InterruptedException {
        SIMPLE_HTTP_CLIENT.init(
//                "http://www.prime-speed.ru/proxy/free-proxy-list/all-working-proxies.php",
//                "http://proxylist.hidemyass.com/search-1299525", // bad?
//                "http://spys.ru/en/free-proxy-list",
//                "http://www.freeproxylists.net/ru/?c=&pt=&pr=&a%5B%5D=0&a%5B%5D=1&a%5B%5D=2&u=90", // bad?
//                "http://proxy-list.org/ru/index.php",
//                "http://webanet.ucoz.ru/publ/24",
//                "http://ninjaseotools.com/free-proxy-list.php",
//                "https://nordvpn.com/free-proxy-list/?country=&ports=&speed%5B1%5D=on&proto%5BHTTP%5D=on&proto%5BHTTPS%5D=on&by=c&order=ASC&perpage=500",
//                "http://www.xroxy.com/proxylist.php?port=&type=All_http&ssl=&country=&latency=1000&reliability=9000#table",
//                "http://freeproxylist.co/",
//                "http://www.ultraproxies.com/http.html%20?lastSort=anonlvl&descending=&order=chg&sort=seconds",
//                "http://www.cool-proxy.net/proxies/http_proxy_list/sort:response_time_average/direction:asc", // bad?
//                "http://www.proxynova.com/proxy-server-list/country-us/",
//                "http://proxylist.sakura.ne.jp/", // bad?
//                "http://proxy-ip-list.com/",
//                "http://seprox.ru/ru/proxy_filter/0_1_0_0_0_0_0_0_1_0.html",
//                "http://checkerproxy.net/all_proxy",
//                "http://www.echolink.org/proxylist.jsp",
//                "http://proxydb.ru/ru-ru/daily-proxy",
//                "http://http-proxy-list.ru/",
//                "http://www.us-proxy.org/",
                "http://free-proxy.cz/en/proxylist/country/all/http/ping/all/1"
        );
        SIMPLE_HTTP_CLIENT.checkProxies();
        System.out.println(SIMPLE_HTTP_CLIENT.getHtmlViaProxy("https://ya.ru"));
    }
}